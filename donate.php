<?php
require "misc/header.php";

// Feel free to add your donation options here, but please don't remove mine.
?>

<title>AstianGO - Donate</title>
<body>
<div class="misc-container">
    <h1>Donate to the <a href="https://astian.org" target="_blank">Astian Team</a></h1>
    <h3>Bitcoin (BTC):</h3>
    <p></p>
    <h3>Monero (XMR):</h3>
    <p></p>
</div>

<?php require "misc/footer.php"; ?>
